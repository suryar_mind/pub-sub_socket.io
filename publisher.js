
window.onload = () => {

    const title = document.getElementById('title');
    const description = document.getElementById('message');
    const channel = document.getElementById('channel');
    const button = document.getElementById('submitBtn');
    const errorDiv = document.getElementById('errorSpan');
    const url = 'http://localhost:3000';

    const socket = io(url);

    socket.on('disconnect', () => {
        console.log('Disconnected');
    });

    socket.on('connect', () => {
        console.log('Connected to Server');
    });

    //Received from Server
    socket.on('message', (message) => {
        console.log('Received Message from Server');
        console.log(message);
    });

    // Send a new broadcasting message
    const sendMessage = (message) => {
        socket.emit('message', {"status": 200, "body": message});
    };

    button.addEventListener('click', (event) => {
        errorDiv.innerText = '';

        if(
            ('' === (title.value)) ||
            ('' === (description.value)) ||
            ('' === (channel.value))
        ) {
            errorDiv.innerText = 'Please fill all details';
        } else {
            sendMessage({"title": title.value, "bodyMessage": description.value, "channelName": channel.value});
            errorDiv.innerText = 'Sent broadcasting message';
        }
    });

};


