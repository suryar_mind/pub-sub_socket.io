
window.onload = () => {

    const alertContainer = document.getElementById('alertContainer1');
    const newChannel = document.getElementById('newChannel');
    const subscribeBtn = document.getElementById('subscribeBtn');
    const url = 'http://localhost:3000';

    const socket = io(url, {
        transports: [ 'websocket' ]
    });

    socket.on('disconnect', () => {
        console.log('Disconnected');
    });

    socket.on('connect', () => {
        console.log('Connected');
    });

    //Received from Server
    socket.on('message', (message) => {
        console.log('Received Message');
    });

    //Received from Server
    socket.on('broadcast', (message) => {
        console.log('Received broadcast Message');
        render(message.body.body);
    });

    // Subscribe to to channels
    const subscribe = (channelName) => {
        if(!isSubscribed(channelName)) {
            let channelList = localStorage.getItem('channels') ?
                JSON.parse(localStorage.getItem('channels')) : [];
            channelList.push(channelName);
            localStorage.setItem('channels', JSON.stringify(channelList));
            console.log('Channel Subscribed');
        } else {
            console.log('Channel already subscribed');
        }
    };

    // Rendering response to HTML
    const render = (message) => {
        if (isSubscribed(message.channelName)) {
            const html = document.createElement('div');
            html.innerHTML = `
                <div class="alert alert-success">
                    <strong>Title: ${message.title}</strong>
                    <br>
                    <p>Message: ${message.bodyMessage}</p>
                    <br>
                    <a>Channel: ${message.channelName}</a>
                </div>
            `;
            alertContainer.insertAdjacentElement('afterbegin', html);
        }
    };

    // To check whether you are Subscribed or not
    const isSubscribed = (channelName) => {
        let channelList = localStorage.getItem('channels') ?
            JSON.parse(localStorage.getItem('channels')) : [];
        return -1 !== channelList.indexOf(channelName);
    };

    subscribeBtn.addEventListener('click', (event) => {
        if ('' !== newChannel.value) {
            subscribe(newChannel.value);
            newChannel.value = '';
        } else {
            console.log('Please provide Channel name');
        }
    });

};


