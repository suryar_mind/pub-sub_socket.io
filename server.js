const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);
const port = 3000;

server.listen(port);
console.log(`Server running on Port ${port}`);

io.sockets.on('connection', function (socket) {

    //Initial Response from Server
    console.log(`Connected IP: ${socket.handshake.headers.host}`);
    socket.emit('message', {"status": 200, "text": "Connected to Server"});

    // Forward Publishers' messages to Subscribers
    socket.on('message', function (message) {
        console.log('Message Received from Publisher');
        broadcast(message);
        console.log('Message broadcast to Subscribers')
    });

    // Broadcast message received from Publisher to all Subscribers
    const broadcast = (message) => {
        const response = {"status": 200, "body": message};
        socket.broadcast.emit('broadcast', response);
    };

});

io.sockets.on('disconnect', function () {
    console.log('Disconnected');
});
